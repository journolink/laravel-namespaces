# Laravel Namespaces

Customise your default namespaces for generated classes in Laravel applications.

**Note:** Currently tested and working against Laravel 5.8 installs

## Install

```bash
composer require journolink/laravel-namespaces
```

Auto-discovery is enabled. If discovery does not run automatically, run:

```bash
php artisan package:discover
```

Finally, publish the configuration by running:

```bash
php artisan vendor:publish --provider="JournoLink\LaravelNamespaces\NamespacesServiceProvider"
```

## Configuration

Configure your default class namespaces in the `config/namespaces.php` file.
