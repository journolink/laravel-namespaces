<?php

namespace JournoLink\LaravelNamespaces;

use Illuminate\Support\ServiceProvider;
use JournoLink\LaravelNamespaces\Commands\JobMakeCommand;
use JournoLink\LaravelNamespaces\Commands\MailMakeCommand;
use JournoLink\LaravelNamespaces\Commands\RuleMakeCommand;
use JournoLink\LaravelNamespaces\Commands\EventMakeCommand;
use JournoLink\LaravelNamespaces\Commands\ModelMakeCommand;
use JournoLink\LaravelNamespaces\Commands\PolicyMakeCommand;
use JournoLink\LaravelNamespaces\Commands\ChannelMakeCommand;
use JournoLink\LaravelNamespaces\Commands\ConsoleMakeCommand;
use JournoLink\LaravelNamespaces\Commands\RequestMakeCommand;
use JournoLink\LaravelNamespaces\Commands\ListenerMakeCommand;
use JournoLink\LaravelNamespaces\Commands\ObserverMakeCommand;
use JournoLink\LaravelNamespaces\Commands\ProviderMakeCommand;
use JournoLink\LaravelNamespaces\Commands\ResourceMakeCommand;
use JournoLink\LaravelNamespaces\Commands\ExceptionMakeCommand;
use JournoLink\LaravelNamespaces\Commands\ControllerMakeCommand;
use JournoLink\LaravelNamespaces\Commands\MiddlewareMakeCommand;
use JournoLink\LaravelNamespaces\Commands\NotificationMakeCommand;

class NamespacesServiceProvider extends ServiceProvider
{
    /**
     * The commands to be registered.
     *
     * @var array
     */
    protected $commands = [
        ChannelMakeCommand::class => 'command.channel.make',
        ConsoleMakeCommand::class => 'command.console.make',
        ControllerMakeCommand::class => 'command.controller.make',
        EventMakeCommand::class => 'command.event.make',
        ExceptionMakeCommand::class => 'command.exception.make',
        JobMakeCommand::class => 'command.job.make',
        ListenerMakeCommand::class => 'command.listener.make',
        MailMakeCommand::class => 'command.mail.make',
        MiddlewareMakeCommand::class => 'command.middleware.make',
        ModelMakeCommand::class => 'command.model.make',
        NotificationMakeCommand::class => 'command.notification.make',
        ObserverMakeCommand::class => 'command.observer.make',
        PolicyMakeCommand::class => 'command.policy.make',
        ProviderMakeCommand::class => 'command.provider.make',
        RequestMakeCommand::class => 'command.request.make',
        ResourceMakeCommand::class => 'command.resource.make',
        RuleMakeCommand::class => 'command.rule.make',
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/namespaces.php' => config_path('namespaces.php'),
        ]);

        $this->mergeConfigFrom(__DIR__.'/../config/namespaces.php', 'namespaces');

        foreach ($this->commands as $class => $abstract) {
            $this->app->extend(
                $abstract,
                function ($concrete, $app) use ($class) {
                    return new $class($app['files']);
                }
            );
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array_values($this->commands);
    }
}