<?php

namespace JournoLink\LaravelNamespaces\Commands;

use Illuminate\Foundation\Console\JobMakeCommand as BaseCommand;

class JobMakeCommand extends BaseCommand
{
    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return rtrim($rootNamespace.'\\'. trim(config('namespaces.jobs'), '\\'), '\\');
    }
}