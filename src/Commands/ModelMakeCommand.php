<?php

namespace JournoLink\LaravelNamespaces\Commands;

use Illuminate\Foundation\Console\ModelMakeCommand as BaseCommand;

class ModelMakeCommand extends BaseCommand
{
    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return rtrim($rootNamespace.'\\'. trim(config('namespaces.models'), '\\'), '\\');
    }
}