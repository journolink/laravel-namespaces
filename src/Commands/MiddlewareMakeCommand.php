<?php

namespace JournoLink\LaravelNamespaces\Commands;

use Illuminate\Routing\Console\MiddlewareMakeCommand as BaseCommand;

class MiddlewareMakeCommand extends BaseCommand
{
    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return rtrim($rootNamespace.'\\'. trim(config('namespaces.middleware'), '\\'), '\\');
    }
}