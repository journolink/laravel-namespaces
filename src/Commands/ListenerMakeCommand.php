<?php

namespace JournoLink\LaravelNamespaces\Commands;

use Illuminate\Foundation\Console\ListenerMakeCommand as BaseCommand;

class ListenerMakeCommand extends BaseCommand
{
    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return rtrim($rootNamespace.'\\'. trim(config('namespaces.listeners'), '\\'), '\\');
    }
}