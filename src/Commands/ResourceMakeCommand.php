<?php

namespace JournoLink\LaravelNamespaces\Commands;

use Illuminate\Foundation\Console\ResourceMakeCommand as BaseCommand;

class ResourceMakeCommand extends BaseCommand
{
    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return rtrim($rootNamespace.'\\'. trim(config('namespaces.resources'), '\\'), '\\');
    }
}