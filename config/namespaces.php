<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Namespaces
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default namespaces to be used by the built-in
    | make commands when creating new classes via Artisan commands. The
    | namespace may still be overridden when in the normal way.
    |
    */

    'channels'      => 'Broadcasting',
    'commands'      => 'Console\Commands',
    'controllers'   => 'Http\Controllers',
    'events'        => 'Events',
    'exceptions'    => 'Exceptions',
    'jobs'          => 'Jobs',
    'listeners'     => 'Listeners',
    'mail'          => 'Mail',
    'middleware'    => 'Http\Middleware',
    'models'        => '',
    'notifications' => 'Notifications',
    'observers'     => 'Observers',
    'policies'      => 'Policies',
    'providers'     => 'Providers',
    'requests'      => 'Http\Requests',
    'resources'     => 'Http\Resources',
    'rules'         => 'Rules',

];